const booking = require('./app');

test('booking  1 room, arrivals = [1, 3, 5], departures = [2, 6, 10] fail', () => {
  let arrivals = [1, 3, 5], departures = [2, 6, 10], k = 1
  expect(booking(arrivals, departures, k)).toBe(0);
});

test('booking  1 room, arrivals = [1, 3, 7], departures = [2, 6, 10] successful', () => {
    const arrivals = [1, 3, 7]
    const departures = [2, 6, 10]
    const k = 1
    expect(booking(arrivals, departures, k)).toBe(1);
  });