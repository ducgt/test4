
  function booking(arrivals, departures, k) {

    if(arrivals === null || arrivals === undefined || arrivals.length === 0 || !Array.isArray(arrivals) ||
        departures === null || departures === undefined || departures.length === 0 || !Array.isArray(departures) ||
        arrivals.length != departures.length ) return -1;
    // Sort both collections
    arrivals.sort(function(a, b){return a-b})
    departures.sort(function(a, b){return a-b})
 
  
    // Number of booking
    let n = arrivals.length;
  
    let count = 0;
    let indexArrival = 0;
    let indexDeparture = 0;
  
    while(indexArrival < n && indexDeparture < n) {
      // Check the min
        if (arrivals[indexArrival] < departures[indexDeparture]) {
        indexArrival++;
        count++;
  
        // If the current booking exceeds the maximum number of rooms
        if (count > k) {
          return 0;
        }
      } else {
 
        indexDeparture++;
        count--;
      }
    }
  
    return 1;
  }

  module.exports = booking;